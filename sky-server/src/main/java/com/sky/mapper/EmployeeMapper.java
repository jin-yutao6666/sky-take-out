package com.sky.mapper;

import com.sky.autofill.AutoFill;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    

    @Select("select * from employee where username = #{username}")
    Employee login(String username);

    @AutoFill(OperationType.INSERT)
    @Insert("insert into employee ( name, username, password, phone, sex, id_number, status, create_time, update_time, create_user, update_user) " +
            "VALUES (#{name},#{username},#{password},#{phone},#{sex},#{idNumber},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insertEmp(Employee employee1);

    List<Employee> findEmpList(String name);


    @AutoFill(OperationType.UPDATE)
    void updateEmp(Employee employee);

    @Select("select * from employee where id =#{id}")
    Employee findById(Long id);

    @AutoFill(OperationType.UPDATE)
    void updatePassword(PasswordEditDTO passwordEditDTO);
}
