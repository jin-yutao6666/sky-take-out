package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {

    List<ShoppingCart> selectById(ShoppingCart shoppingCart);


    void update(ShoppingCart shoppingCart);

    void insert(ShoppingCart shoppingCart1);

    @Select("select * from shopping_cart ")
    List<ShoppingCart> findCartList();


    void delCartList(ShoppingCart shoppingCart);
}
