package com.sky.mapper;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface UserMapper {

   @Select("select * from user where openid =#{openid}")
    User findOpenId(String openid);

   @Insert("insert into user (id, openid, name, phone, sex, id_number, avatar, create_time) VALUES  " +
           "(#{id},#{openid},#{name},#{phone},#{sex},#{idNumber},#{avatar},#{createTime})")
    void insert(User user);

    List<UserReportDTO> countByUserTime(LocalDateTime beginTime, LocalDateTime endTime);

    @Select("select count(*) from user where create_time < #{beginTime}")
    Long findUserCount(LocalDateTime beginTime);
}
