package com.sky.mapper;


import com.sky.autofill.AutoFill;
import com.sky.entity.SetmealDish;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


@Mapper
public interface SetmealDishMapper {

    List<Long> findList(List<Long> ids);



    Long findSetmealId(Long id);


    void save(List<SetmealDish> setmealDishes);

    void delSetmealDish(List<Integer> ids);

    @Select("select * from setmeal_dish where setmeal_id =#{id}")
    List<SetmealDish> findDishList(Integer id);
}
