package com.sky.mapper;


import com.sky.autofill.AutoFill;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface SetmealMapper {

    @Select("select * from setmeal where category_id =#{categoryId}")
    List<Setmeal> selectSetmealId(Long categoryId);


    void updatestatusBySetmealId(Long setmealId, Integer status);

    @AutoFill(OperationType.INSERT)
    @Options(useGeneratedKeys = true , keyProperty = "id")
    @Insert("insert into setmeal (name, category_id, price, description,status, image, create_time, update_time, create_user, update_user) VALUES  " +
            "(#{name},#{categoryId},#{price},#{description},#{status},#{image},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void save(Setmeal setmeal);

    List<SetmealVO> findSetmealList(SetmealPageQueryDTO setmealPageQueryDTO);


    Long findStatus(List<Integer> ids);

    void delSetmeal(List<Integer> ids);

    @Select("select * from setmeal where id= #{id}")
    SetmealVO findSetmeal(Integer id);

    @AutoFill(OperationType.UPDATE)
    void updateSetmeal(Setmeal setmeal);
}
