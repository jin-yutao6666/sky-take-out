package com.sky.mapper;

import com.sky.autofill.AutoFill;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishFlavorsMapper {

    void save(List<DishFlavor> flavors);

    void delDishFlavor(List<Long> ids);

    @Select("select * from dish_flavor where dish_id =#{id}")
    List<DishFlavor> findDishFlavors(Integer id);
}
