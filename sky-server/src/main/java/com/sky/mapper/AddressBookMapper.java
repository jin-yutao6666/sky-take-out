package com.sky.mapper;

import com.sky.entity.AddressBook;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AddressBookMapper {

    void save(AddressBook addressBook);

    @Select("select * from address_book")
    List<AddressBook> findList();

    @Select("select * from address_book where id = #{id}")
    AddressBook findById(Long id);

    void updateAddressList(AddressBook addressBook);

    @Delete("delete from address_book where id =#{id}")
    void delAddressList(Long id);

    @Select("select * from address_book where is_default = #{i}")
    AddressBook findDefault(int i);

    @Select("select * from address_book where user_id =#{currentId}")
    AddressBook findDefultByUserId(Long currentId);
}
