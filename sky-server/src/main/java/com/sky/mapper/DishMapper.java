package com.sky.mapper;

import com.sky.autofill.AutoFill;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishMapper {

   @Select("select * from dish where category_id = #{categoryId}")
    List<Dish> select(Long categoryId);

   @AutoFill(value = OperationType.INSERT)
   @Options(useGeneratedKeys = true, keyProperty = "id")
   @Insert("insert into dish (name, category_id, price, image, description, create_time, update_time, create_user, update_user) " +
           "VALUES(#{name},#{categoryId},#{price},#{image},#{description},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void save(Dish dish);


    List<DishVO> findDish(DishPageQueryDTO dishPageQueryDTO);


    Long findDishStastus(List<Long> ids);

    void delDish(List<Long> ids);

    @Select("select * from dish where id = #{id}")
    DishVO findDishById(Integer id);

    @AutoFill(OperationType.UPDATE)
    void updateDish(Dish dish);

    List<Dish> findList(DishPageQueryDTO dishPageQueryDTO);

    Long findDishStastusById(Long id);


}
