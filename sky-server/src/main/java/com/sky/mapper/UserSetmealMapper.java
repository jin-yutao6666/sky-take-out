package com.sky.mapper;

import com.sky.dto.SetmealDTO;
import com.sky.entity.Setmeal;
import com.sky.vo.DishItemVO;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserSetmealMapper {

    List<Setmeal> setmealList(SetmealDTO setmealDTO);

    List<DishItemVO> setmealDishList(Long id);
}
