package com.sky.mapper;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface ReportMapper {


    List<TurnoverReportDTO> selectStatistics(Integer status, LocalDateTime beginTime, LocalDateTime endTime);

    List<OrderReportDTO> findCountOrder(LocalDateTime beginTime, LocalDateTime endTime,Integer status);

    Integer countOrderByTime(LocalDateTime beginTime, LocalDateTime endTime, Integer status);

    List<SalesReportDTO> findTopDish(LocalDateTime beginTime, LocalDateTime endTime);
}
