package com.sky.mapper;

import com.sky.entity.BusinessData;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;

@Mapper
public interface AdminWorkSpaceMapper {

   @Select("select count(*) from user where create_time between #{beginTime} and #{endTime}")
    Integer newUser(LocalDateTime beginTime, LocalDateTime endTime);

    DishOverViewVO findDishStatus();

    SetmealOverViewVO findSetmealsStautus();

    BusinessData findReport(LocalDateTime beginTime, LocalDateTime endTime);

    OrderOverViewVO findOrder(LocalDateTime beginTime, LocalDateTime endTime);
}
