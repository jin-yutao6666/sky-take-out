package com.sky.mapper;

import com.sky.autofill.AutoFill;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

@Mapper
public interface CategoryMapper {

    @AutoFill(OperationType.INSERT)
    @Insert("insert into category ( type, name, sort,  create_time, update_time, create_user, update_user) VALUES " +
            "(#{type},#{name},#{sort},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insertCategory(Category category);

    List<Category> findCategoryList(String name, Integer type);

    @Select("select * from category where id =#{id}")
    Category findCategoryId(Long id);

    @AutoFill(OperationType.UPDATE)
    void updateCategroy(Category category);

    @Delete("delete from category where id =#{id}")
    void delCategroy(Long id);

    @Select("select * from category where type =#{type} order by create_time desc ")
    List<Category> findCategoryByType(Integer type);

    @Select("select * from category")
    List<Category> find();
}
