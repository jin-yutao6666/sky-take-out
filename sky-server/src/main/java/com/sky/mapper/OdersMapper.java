package com.sky.mapper;

import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface OdersMapper {

    void insert(Orders orders);

    List<OrderVO> findOrdersList(Orders orders);

    void update(OrderVO orderVO);


    OrderStatisticsVO findOrderStatus();

    @Select("select * from orders where order_time <=#{orderTimeBefore} and status =#{status}")
    List<Orders> findOrderTimeBefore(Integer status, LocalDateTime orderTimeBefore);
}
