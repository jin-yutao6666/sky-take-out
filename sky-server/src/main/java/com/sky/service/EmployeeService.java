package com.sky.service;


import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.result.PageResult;

public interface EmployeeService {

     PageResult findEmpList(EmployeePageQueryDTO pageQueryDTO);

    //查询登录管理员
    Employee login(EmployeeLoginDTO employeeLoginDTO);
    //新增员工
    void save(EmployeeDTO employeeDTO);

    void changeStatus(Integer status, Long id);

    Employee findById(Long id);

    void updateEmp(Employee employee);

    void updatePassword(PasswordEditDTO passwordEditDTO);
}
