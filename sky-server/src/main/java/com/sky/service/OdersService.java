package com.sky.service;

import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.entity.Orders;
import com.sky.result.PageResult;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;

public interface OdersService {

    OrderSubmitVO OderSubmit(Orders orders);

    PageResult findHistoryOrders(OrdersPageQueryDTO ordersPageQueryDTO);

    OrderVO QueryOrder(Long id);

    void addOrder(Long id);

    void celOrders(Long id);

    OrderStatisticsVO findOrderStatus();

    OrderVO findOrderDetail(Integer id);

    void ConfirmOrders(Orders orders);

    void RejectionOder(OrdersRejectionDTO ordersRejectionDTO);

    void cancelOrders(OrdersCancelDTO ordersCancelDTO);

    void Delivery(Long id);

    void CompleteOrders(Long id);
}
