package com.sky.service;

import com.sky.dto.CategoryDTO;
import com.sky.entity.Category;

import java.util.List;

public interface UserCategoryService {

    List<Category> categoryList(CategoryDTO categoryDTO);
}
