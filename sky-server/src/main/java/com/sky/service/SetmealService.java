package com.sky.service;


import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface SetmealService {


    void save(SetmealDTO setmealDTO);

    PageResult SetmealList(SetmealPageQueryDTO setmealPageQueryDTO);

    void delSetmeal(List<Integer> ids);

    SetmealVO findSetmeal(Integer id);

    void updateSetmeal(SetmealDTO setmealDTO);

    void ableAndDisable(Integer status, Long id);
}
