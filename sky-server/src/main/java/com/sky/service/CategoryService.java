package com.sky.service;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;

import java.util.List;

public interface CategoryService {

 

    void save(CategoryDTO categoryDTO);

    PageResult findCategoryList(CategoryPageQueryDTO categoryPageQueryDTO);

    Category findCategoryId(Long id);

    void updateCategroy(CategoryDTO categoryDTO);

    void ableAndDisable(Integer status, Long id);

    void delCategroy(Long id);


    List<Category> findCategoryByType(Integer type);
}
