package com.sky.service;

import com.sky.entity.AddressBook;

import java.util.List;

public interface AddressBookService {

    void save(AddressBook addressBook);

    List<AddressBook> findList();

    AddressBook findById(Long id);

    void updateAddressList(AddressBook addressBook);

    void delAddressList(Long id);

    void setDefult(AddressBook addressBook);

    AddressBook findDefult();
}
