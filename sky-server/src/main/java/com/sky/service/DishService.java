package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {

    void save(DishDTO dishDTO);

    PageResult findDish(DishPageQueryDTO dishPageQueryDTO);

    void delDish(List<Long> ids);

    DishVO findDishAndFlavors(Integer id);

    void updateDish(DishDTO dishDTO);

    void dishStatus(Integer status, Long id);

    List<Dish> findList(DishPageQueryDTO dishPageQueryDTO);
}
