package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.entity.Setmeal;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;

import java.util.List;

public interface UserSetmealService {

    List<Setmeal> setmealList(SetmealDTO setmealDTO);

    List<DishItemVO> setmealDishList(Long id);
}
