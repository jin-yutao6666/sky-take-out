package com.sky.service.impl;

import com.sky.constant.StatusConstant;
import com.sky.entity.Dish;
import com.sky.mapper.DishFlavorsMapper;
import com.sky.mapper.UserDishMapper;
import com.sky.service.UserDishService;
import com.sky.vo.DishVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDishServiceImpl implements UserDishService {

    @Autowired
    UserDishMapper userDishMapper;
    @Autowired
    DishFlavorsMapper dishFlavorsMapper;
    @Autowired
    RedisTemplate<Object,Object> redisTemplate;

    @Override
    public List<DishVO> UserDishList(Long categoryId) {

        //设置菜品缓存
        //1.查看redis里有没有存在key值
        List<DishVO> dishList = (List<DishVO>) redisTemplate.opsForValue().get("dish:" + categoryId);
        if (CollectionUtils.isEmpty(dishList)) {
            //如果没有去数据库里查
            Dish dish = Dish.builder().categoryId(categoryId).status(StatusConstant.ENABLE).build();
             dishList = userDishMapper.UserDishList(dish);
        }

        //查完先缓存到redis
        redisTemplate.opsForValue().set("dish:"+categoryId,dishList);
        //最后 返回数据
        return dishList;
    }
}
