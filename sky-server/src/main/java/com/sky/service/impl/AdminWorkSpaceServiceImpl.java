package com.sky.service.impl;

import com.sky.entity.BusinessData;
import com.sky.mapper.AdminWorkSpaceMapper;
import com.sky.service.AdminWorkSpaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Service
public class AdminWorkSpaceServiceImpl implements AdminWorkSpaceService {

    @Autowired
    AdminWorkSpaceMapper adminWorkSpaceMapper;

    @Override
    public BusinessDataVO businessData() {

        LocalDate now = LocalDate.now();
        LocalDateTime beginTime = LocalDateTime.of(now, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(now, LocalTime.MAX);



        //营业额 有效订单数 订单完成率 平均客单价
        BusinessData businessData  =adminWorkSpaceMapper.findReport(beginTime,endTime);

       //新增用户数
        Integer newUsers = adminWorkSpaceMapper.newUser(beginTime,endTime);

        return new BusinessDataVO(businessData.getTurnover(),businessData.getValidOrderCount(),
                businessData.getOrderCompletionRate(),businessData.getUnitPrice(),newUsers);
    }

    @Override
    public OrderOverViewVO overviewOrders() {
        LocalDate now = LocalDate.now();
        LocalDateTime beginTime = LocalDateTime.of(now, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(now, LocalTime.MAX);

        //待接单数量
        OrderOverViewVO orderOverViewVO = adminWorkSpaceMapper.findOrder(beginTime, endTime);


        return orderOverViewVO;
    }

    @Override
    public DishOverViewVO overviewDishes() {
        DishOverViewVO dishOverViewVO =adminWorkSpaceMapper.findDishStatus();
        return dishOverViewVO;
    }

    @Override
    public SetmealOverViewVO overviewSetmeals() {
        SetmealOverViewVO setmealOverViewVO =adminWorkSpaceMapper.findSetmealsStautus();
        return setmealOverViewVO;
    }
}
