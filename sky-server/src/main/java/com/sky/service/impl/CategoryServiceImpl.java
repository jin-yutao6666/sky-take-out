package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.CategoryService;
import com.sky.utils.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    DishMapper dishMapper;
    @Autowired
    SetmealMapper setmealMapper;

    Long currentId = BaseContext.getCurrentId();

    @Override
    public void save(CategoryDTO categoryDTO) {
        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
//        category.setCreateTime(LocalDateTime.now());
//        category.setUpdateTime(LocalDateTime.now());
//        Long currentId1 = BaseContext.getCurrentId();
//        category.setCreateUser(currentId1);
//        category.setUpdateUser(currentId1);
        categoryMapper.insertCategory(category);

    }

    @Override
    public PageResult findCategoryList(CategoryPageQueryDTO categoryPageQueryDTO) {
        PageHelper.startPage(categoryPageQueryDTO.getPage(),categoryPageQueryDTO.getPageSize());
        String name = categoryPageQueryDTO.getName();
        Integer type = categoryPageQueryDTO.getType();
      List<Category> categoryList =  categoryMapper.findCategoryList(name,type);

        Page<Category> page = (Page<Category>) categoryList;
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public Category findCategoryId(Long id) {
        Category category = categoryMapper.findCategoryId(id);
        return category;
    }

    @Override
    public void updateCategroy(CategoryDTO categoryDTO) {
        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(currentId);
        categoryMapper.updateCategroy(category);

    }

    @Override
    public void ableAndDisable(Integer status, Long id) {
        Category category = new Category();
        category.setId(id);
        category.setStatus(status);
//        category.setUpdateTime(LocalDateTime.now());
//
//        category.setUpdateUser(currentId);
        categoryMapper.updateCategroy(category);
    }

    @Override
    public void delCategroy(Long id) {
        //查询此id对应的套餐类型
        Category category = categoryMapper.findCategoryId(id);
        Integer type = category.getType();
        Long categoryId = Long.valueOf(type);

        if (categoryId ==1){
            //先判断分类下还有菜品
            selectDish(categoryId);
        }else {
            //先判断分类下还有套餐
            selectSetmeal(categoryId);
        }

        //删除分类
        categoryMapper.delCategroy(id);
    }

    @Override
    public List<Category> findCategoryByType(Integer type) {
        List<Category>  categoryList = categoryMapper.findCategoryByType(type);
        return categoryList;
    }


    private void selectSetmeal(Long categoryId) {
        List<Setmeal>  Setmeallist = setmealMapper.selectSetmealId(categoryId);
        if (Setmeallist.size() > 0){
            throw new BusinessException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
    }

    private void selectDish(Long categoryId) {
        List<Dish> dishList =dishMapper.select(categoryId);
        if (dishList.size()>0){
            throw new BusinessException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }

    }



}
