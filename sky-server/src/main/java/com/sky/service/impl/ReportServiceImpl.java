package com.sky.service.impl;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.BusinessData;
import com.sky.exception.BusinessException;
import com.sky.mapper.AdminWorkSpaceMapper;
import com.sky.mapper.OdersMapper;
import com.sky.mapper.ReportMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.vo.*;
import org.apache.poi.ss.formula.functions.Count;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    ReportMapper reportMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    AdminWorkSpaceMapper adminWorkSpaceMapper;
    @Autowired
    HttpServletResponse response;

    @Override
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {

        List<LocalDate> dateList = begin.datesUntil(end.plusDays(1)).collect(Collectors.toList());

        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

        List<TurnoverReportDTO> turnoverReportDTOList = reportMapper.selectStatistics(5, beginTime, endTime);
        if (turnoverReportDTOList == null) {
            throw new BusinessException("");
        }

        List<BigDecimal> turnoverList = new ArrayList<>();
        turnoverReportDTOList.forEach(turnoverReportDTO -> {
            BigDecimal orderCount = turnoverReportDTO.getOrderMoney();
            if (orderCount != null) {
                turnoverList.add(orderCount);
            }
            turnoverList.add(BigDecimal.valueOf(0));
        });

        return new TurnoverReportVO(dateList, turnoverList);
    }

    @Override
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {
        List<LocalDate> dateList = begin.datesUntil(end).collect(Collectors.toList());

        //每日新增用户数量
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<UserReportDTO> userReportDTOList = userMapper.countByUserTime(beginTime, endTime);

        Map<String, Integer> oderMap = userReportDTOList.stream().collect(Collectors.toMap(UserReportDTO::getCreateDate, UserReportDTO::getUserCount));
        List<Integer> newUserList = dateList.stream().map(date ->
                oderMap.get(date.toString()) == null ? 0 : oderMap.get(date.toString())
        ).collect(Collectors.toList());

        //查询用户总数量
        Long add = userMapper.findUserCount(beginTime);

        List<Long> totalUserList = new ArrayList<>();
        for (Integer newuser : newUserList) {
            add += newuser;
            totalUserList.add(add);
        }


        return new UserReportVO(dateList, totalUserList, newUserList);
    }

    @Override
    public OrderReportVO ordersStatistics(LocalDate begin, LocalDate end) {
        List<LocalDate> localdateList = begin.datesUntil(end.plusDays(1)).collect(Collectors.toList());
        List<String> dateList = localdateList.stream().map(localdate -> {
            return localdate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }).collect(Collectors.toList());

        //每日订单数，
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<OrderReportDTO> orderReportDTOList = reportMapper.findCountOrder(beginTime,endTime,null);
        Map<String, Integer> OrderReportDTOMap = orderReportDTOList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));

        List<Integer> orderCountList = dateList.stream().map(date ->
                OrderReportDTOMap.get(date) == null ? 0 : OrderReportDTOMap.get(date)
        ).collect(Collectors.toList());

        //每日有效订单数
        List<OrderReportDTO> validorderReportDTOList = reportMapper.findCountOrder(beginTime,endTime,6);
        Map<String, Integer> validOrderCountMap = validorderReportDTOList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> validOrderCountList = dateList.stream().map(date ->
             validOrderCountMap.get(date) == null ? 0 : validOrderCountMap.get(date)
        ).collect(Collectors.toList());


        //订单总数
        Integer totalOrderCount = reportMapper.countOrderByTime(beginTime,endTime,null);
        //有效订单数
        Integer validOrderCount = reportMapper.countOrderByTime(beginTime,endTime,6);
        //订单完成率
        Double orderCompletionRate = validOrderCount.doubleValue() /totalOrderCount;
        return new OrderReportVO(dateList,orderCountList,validOrderCountList,totalOrderCount,validOrderCount,orderCompletionRate);
    }

    @Override
    public SalesTop10ReportVO ordersTop(LocalDate begin, LocalDate end) {

        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
       List<SalesReportDTO> salesReportDTOList = reportMapper.findTopDish(beginTime,endTime);

            List<String> nameList = new ArrayList<>();
        List<Integer> goodsNumber = new ArrayList<>();

        for (SalesReportDTO salesReportDTO : salesReportDTOList) {
            String goodsName = salesReportDTO.getGoodsName();
            nameList.add(goodsName);
            Integer goodsNumber1 = salesReportDTO.getGoodsNumber();
            goodsNumber.add(goodsNumber1);
        }


        return new SalesTop10ReportVO(nameList,goodsNumber);
    }

    @Override
    public void excelReport() throws Exception {
        //1.获取时间（近30天的）
        LocalDate begin = LocalDate.now().minusDays(30);
        LocalDate end = LocalDate.now();
        List<LocalDate> dateList = begin.datesUntil(end).collect(Collectors.toList());

        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        //2.查询概览数据
        BusinessData businessData = adminWorkSpaceMapper.findReport(beginTime, endTime);
        //新增用户数
        Integer newUsers = adminWorkSpaceMapper.newUser(beginTime,endTime);


        //3.查询明细数据

        //4.加载表单模板
        String TIME ="日期从 "+ begin.toString() +" 到 "+end.toString();
        InputStream inputStream= this.getClass().getClassLoader().getResource("template/运营数据报表模板.xlsx").openStream();
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
        XSSFSheet sheet = xssfWorkbook.getSheetAt(0);
        sheet.getRow(1).getCell(1).setCellValue(TIME);
        sheet.getRow(3).getCell(2).setCellValue(businessData.getTurnover());
        sheet.getRow(3).getCell(4).setCellValue(businessData.getOrderCompletionRate());
        sheet.getRow(3).getCell(6).setCellValue(newUsers);
        sheet.getRow(4).getCell(2).setCellValue(businessData.getValidOrderCount());
        sheet.getRow(4).getCell(4).setCellValue(businessData.getUnitPrice());

        //5.将数据加入模板

        for (int i = 0; i <30 ; i++) {
            LocalDate date = dateList.get(i);
            LocalDateTime _beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime _endTime = LocalDateTime.of(date, LocalTime.MAX);
            BusinessData _businessData = adminWorkSpaceMapper.findReport(_beginTime, _endTime);
            Integer _newUsers = adminWorkSpaceMapper.newUser(_beginTime,_endTime);
            sheet.getRow(7+i).getCell(1).setCellValue(date.toString());
            sheet.getRow(7+i).getCell(2).setCellValue(_businessData.getTurnover());
            sheet.getRow(7+i).getCell(3).setCellValue(_businessData.getValidOrderCount());
            sheet.getRow(7+i).getCell(4).setCellValue(_businessData.getOrderCompletionRate());
            sheet.getRow(7+i).getCell(5).setCellValue(_businessData.getUnitPrice());
            sheet.getRow(7+i).getCell(6).setCellValue(_newUsers);

        }


        //6.输出文件流
        ServletOutputStream ot = response.getOutputStream();
        xssfWorkbook.write(ot);

        //7.关闭资源
    }
}
