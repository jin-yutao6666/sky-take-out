package com.sky.service.impl;

import com.sky.dto.CategoryDTO;
import com.sky.entity.Category;
import com.sky.mapper.UserCategoryMapper;
import com.sky.service.UserCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserCategoryServiceImpl implements UserCategoryService {

    @Autowired
    UserCategoryMapper userCategoryMapper;

    @Override
    public List<Category> categoryList(CategoryDTO categoryDTO) {
        List<Category> categoryList = userCategoryMapper.categoryList(categoryDTO);
        return categoryList;
    }
}
