package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.SetmealService;
import com.sky.service.ShoppingCartService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;


@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    ShoppingCartMapper shoppingCartMapper;
    @Autowired
    DishMapper dishMapper;
    @Autowired
    SetmealMapper setmealMapper;

    @Override
    public void save(ShoppingCartDTO shoppingCartDTO) {
        //1.先判断是菜品还是套餐，然后根据dishid/categoryId查询返回

        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        Long currentId = BaseContext.getCurrentId();
        shoppingCart.setUserId(currentId);

        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectById(shoppingCart);
        if (!CollectionUtils.isEmpty(shoppingCarts)){
            //如果dish或category存在就数量加一
            ShoppingCart cart = shoppingCarts.get(0);
            cart.setNumber(cart.getNumber()+1);
            shoppingCartMapper.update(cart);
        }else {
            //2.，不存在就插入
            Long dishId = shoppingCart.getDishId();
            if (dishId != null){ //菜品
                DishVO dish = dishMapper.findDishById(Math.toIntExact(dishId));
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setAmount(dish.getPrice());
                shoppingCart.setName(dish.getName());

            }else {  //套餐
                SetmealVO setmeal = setmealMapper.findSetmeal(Math.toIntExact(shoppingCart.getSetmealId()));
                shoppingCart.setImage(setmeal.getImage());
                shoppingCart.setAmount(setmeal.getPrice());
                shoppingCart.setName(setmeal.getName());

            }
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartMapper.insert(shoppingCart);


        }


    }

    @Override
    public List<ShoppingCart> find() {

        List<ShoppingCart> cartList = shoppingCartMapper.findCartList();
        return cartList;
    }

    @Override
    public void updateCartList(ShoppingCart shoppingCart) {
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectById(shoppingCart);
        ShoppingCart shoppingCart1 = shoppingCartList.get(0);
        Integer number = shoppingCart1.getNumber();
        if (number>1){
            shoppingCart.setNumber(number-1);
            shoppingCartMapper.update(shoppingCart);
        }else {
            shoppingCartMapper.delCartList(shoppingCart);
        }

    }

    @Override
    public void delCartList() {
        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();
        shoppingCartMapper.delCartList(shoppingCart);
    }
}
