package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.entity.AddressBook;
import com.sky.mapper.AddressBookMapper;
import com.sky.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AddressBookServiceImpl implements AddressBookService {

    @Autowired
    AddressBookMapper addressBookMapper;

    @Override
    public void save(AddressBook addressBook) {
        addressBook.setCreateTime(LocalDateTime.now());
        addressBook.setUserId(BaseContext.getCurrentId());
        addressBookMapper.save(addressBook);
    }

    @Override
    public List<AddressBook> findList() {
        List<AddressBook> addressBookList = addressBookMapper.findList();
        return addressBookList;
    }

    @Override
    public AddressBook findById(Long id) {
        AddressBook addressBook = addressBookMapper.findById(id);
        return addressBook;
    }

    @Override
    public void updateAddressList(AddressBook addressBook) {
        addressBookMapper.updateAddressList(addressBook);
    }

    @Override
    public void delAddressList(Long id) {
        addressBookMapper.delAddressList(id);
    }

    @Override
    public void setDefult(AddressBook addressBook) {
        //查询当前默认地址
        AddressBook addressBook1 = addressBookMapper.findDefault(1);
        Long currentId = BaseContext.getCurrentId();
        //将当前默认地址修改
        if (addressBook1 != null){
            addressBook1.setIsDefault(0);
            addressBookMapper.updateAddressList(addressBook1);
        }
        //再将传入id的地址改为默认
        AddressBook addressBook2 = AddressBook.builder().id(addressBook.getId()).isDefault(1).userId(currentId).build();
        addressBookMapper.updateAddressList(addressBook2);
    }

    @Override
    public AddressBook findDefult() {
        Long currentId = BaseContext.getCurrentId();
        AddressBook addressBook = addressBookMapper.findDefultByUserId(currentId);
        return addressBook;
    }
}
