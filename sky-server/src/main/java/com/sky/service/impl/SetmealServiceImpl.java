package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishFlavorsMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.service.SetmealService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.LongValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    SetmealMapper setmealMapper;
    @Autowired
    SetmealDishMapper setmealDishMapper;
    @Autowired
    DishMapper dishMapper;


    @CacheEvict(cacheNames = "cache:setmeal",key = "#a0.categoryId")
    @Transactional
    @Override
    public void save(SetmealDTO setmealDTO) {
        //1.套餐基础字段保存至套餐表
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        setmeal.setStatus(0);
        setmealMapper.save(setmeal);
        //2.将菜品保存到菜品套餐中间表
        Long setmealId = setmeal.getId();
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        setmealDishes.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmealId);
        });
        setmealDishMapper.save(setmealDishes);

    }

    @Override
    public PageResult SetmealList(SetmealPageQueryDTO setmealPageQueryDTO) {
            PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());

           List<SetmealVO> setmealList = setmealMapper.findSetmealList(setmealPageQueryDTO);
            Page<SetmealVO> page = (Page<SetmealVO>) setmealList;

        return new PageResult(page.getTotal(),page.getResult());

    }

    @CacheEvict(cacheNames = "cache:setmeal", allEntries = true)
    @Transactional
    @Override
    public void delSetmeal(List<Integer> ids) {
        //启售中的套餐不能删除
        Long statusCout=setmealMapper.findStatus(ids);
        if (statusCout > 0 ){
            throw new BusinessException(MessageConstant.SETMEAL_ON_SALE);
        }

        // 不仅删除套餐基本信息，还有删除套餐菜品关联表信息

        setmealMapper.delSetmeal(ids);
        setmealDishMapper.delSetmealDish(ids);


    }

    @Override
    public SetmealVO findSetmeal(Integer id) {
        SetmealVO setmealVO = setmealMapper.findSetmeal(id);

       List<SetmealDish> dishList= setmealDishMapper.findDishList(id);

        setmealVO.setSetmealDishes(dishList);
        return setmealVO;
    }

    @CacheEvict(cacheNames = "cache:setmeal", allEntries = true)
    @Transactional
    @Override
    public void updateSetmeal(SetmealDTO setmealDTO) {
        //先修改Setmeal字段
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        setmealMapper.updateSetmeal(setmeal);

        //修改 SetmealDish 字段（先删 后添加）
        //删除与此套餐相关菜品
        long id =setmeal.getId();
        Integer id1 = Math.toIntExact(id);
        List<Integer> ids = new ArrayList<>();
        ids.add(id1);
        setmealDishMapper.delSetmealDish(ids);

        //重新添加此套餐相关菜品
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        setmealDishes.forEach(setmealDish -> {
            setmealDish.setSetmealId(id);
        });
        setmealDishMapper.save(setmealDishes);



    }

    @CacheEvict(cacheNames = "cache:setmeal",allEntries = true)
    @Override
    public void ableAndDisable(Integer status, Long id) {
        //先判断此操作是启售还是停售
        if (status.equals(1) ){
            //判断此套餐关联菜品是否有停售，如果有不允许启售
            Long count=dishMapper.findDishStastusById(id);
            if (count>0){
                throw new BusinessException(MessageConstant.SETMEAL_ENABLE_FAILED);
            }
        }

        Setmeal setmeal = new Setmeal();
        setmeal.setStatus(status);
        setmeal.setId(id);
        setmealMapper.updateSetmeal(setmeal);


    }
}
