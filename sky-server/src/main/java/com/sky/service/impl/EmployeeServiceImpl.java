package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.exception.BusinessException;
import com.sky.exception.DataException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.utils.BeanHelper;
import io.netty.util.internal.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
   private EmployeeMapper employeeMapper;

    @Autowired
    RedisTemplate redisTemplate;


    //查询员工
    @Override
    public PageResult findEmpList(EmployeePageQueryDTO pageQueryDTO) {
        PageHelper.startPage(pageQueryDTO.getPage(), pageQueryDTO.getPageSize());
        List<Employee> empList  =employeeMapper.findEmpList(pageQueryDTO.getName());

        Page<Employee> page = (Page<Employee>) empList;
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
            log.info("输入用户 {}",employeeLoginDTO);
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();


        //校验账户是否被锁定
        Object o = redisTemplate.opsForValue().get("login" + username);
        if (ObjectUtils.isNotEmpty(o)){
            throw new BusinessException(MessageConstant.ACCOUNT_LOCKED);
        }

        // 1、根据用户名查询员工信息
        Employee employee = employeeMapper.login(username);

        //2.校验员工是否存在
        if (employee==null){
            throw new DataException(MessageConstant.ACCOUNT_NOT_FOUND);
        }
        //校验员工密码是否正确
        if (!password.equals(employee.getPassword())){



        //记录员工密码错误的标记，并设置有效期为5分钟
            redisTemplate.opsForValue().set("login:"+username+":"+ RandomStringUtils.randomAlphabetic(5),"-",5, TimeUnit.MINUTES);
        //获取改员工的密码错误标记，如果标记的数量大于=5 设置账号锁的标记
            Set keys = redisTemplate.keys("login:" + username + ":*");
            if ( keys != null  && keys.size()>=5){

                log.info("密码或账号连续输错5次，账号冻结1小时");
                redisTemplate.opsForValue().set("login"+username,"-",1,TimeUnit.HOURS);
                throw new BusinessException(MessageConstant.LOGIN_BLOCK);
            }


            throw new BusinessException(MessageConstant.PASSWORD_ERROR);
        }
        //校验员工是否被禁用
        if (employee.getStatus() == StatusConstant.DISABLE){
            throw new BusinessException(MessageConstant.ACCOUNT_LOCKED);
        }

        return employee;
    }

    //新增员工
    @Override
    public void save(EmployeeDTO employeeDTO) {
        log.info("新增员工信息，{}",employeeDTO);

        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);

        employee.setPassword("123456");
        employee.setStatus(1);
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());

        //获取threadlocal里面的empid
//        Long currentId = BaseContext.getCurrentId();
//        employee.setCreateUser(currentId);
//        employee.setUpdateUser(currentId);

        employeeMapper.insertEmp(employee);


    }

    @Override
    public void changeStatus(Integer status, Long id) {
        Long currentId = BaseContext.getCurrentId();
        Employee employee = new Employee();
        employee.setId(id);
        employee.setStatus(status);
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(currentId);
        employeeMapper.updateEmp(employee);
    }

    //查询回显id
    @Override
    public Employee findById(Long id) {
        Employee employee =employeeMapper.findById(id);
        return employee;
    }

    @Override
    public void updateEmp(Employee employee) {
//        Long currentId = BaseContext.getCurrentId();
//        employee.setUpdateUser(currentId);
        employeeMapper.updateEmp(employee);
    }

    @Override
    public void updatePassword(PasswordEditDTO passwordEditDTO) {
//        Long empId = BaseContext.getCurrentId();
//        passwordEditDTO.setEmpId(empId);
        employeeMapper.updatePassword(passwordEditDTO);

    }
}
