package com.sky.service.impl;

import com.sky.dto.SetmealDTO;
import com.sky.entity.Setmeal;
import com.sky.mapper.UserSetmealMapper;
import com.sky.service.UserSetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserSetmealServiceImpl implements UserSetmealService {

    @Autowired
    UserSetmealMapper userSetmealMapper;


    @Cacheable(cacheNames = "cache:setmeal",key = "#a0.categoryId")
    @Override
    public List<Setmeal> setmealList(SetmealDTO setmealDTO) {

        List<Setmeal> setmealList = userSetmealMapper.setmealList(setmealDTO);

        return setmealList;
    }


    @Override
    public List<DishItemVO> setmealDishList(Long id) {
        List<DishItemVO> dishItemVOList = userSetmealMapper.setmealDishList(id);
        return dishItemVOList;
    }
}
