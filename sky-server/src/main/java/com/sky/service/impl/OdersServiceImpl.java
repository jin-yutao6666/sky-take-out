package com.sky.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.controller.WebScoketServer;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.entity.AddressBook;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.entity.ShoppingCart;
import com.sky.exception.BusinessException;
import com.sky.mapper.AddressBookMapper;
import com.sky.mapper.OdersDetailMapper;
import com.sky.mapper.OdersMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.result.PageResult;
import com.sky.service.OdersService;
import com.sky.utils.BaiduLocationUtil;
import com.sky.utils.BeanHelper;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OdersServiceImpl implements OdersService {

   private static String ads2 = "湖北省武汉市黄陂区青龙路传智播客教育科创园";
   @Autowired
   private BaiduLocationUtil baiduLocationUtil;

    @Autowired
    OdersMapper odersMapper;
    @Autowired
    AddressBookMapper addressBookMapper;
    @Autowired
    ShoppingCartMapper shoppingCartMapper;
    @Autowired
    OdersDetailMapper odersDetailMapper;
    @Autowired
    WebScoketServer webScoketServer;
    @Override
    public OrderSubmitVO OderSubmit(Orders orders) {

        //根据地址id查询地址信息
        Long addressBookId = orders.getAddressBookId();
        AddressBook addressBook = addressBookMapper.findById(addressBookId);

        //先判断地址是否在配送范围内
        String provinceName = addressBook.getProvinceName();
        String cityName = addressBook.getCityName();
        String detail = addressBook.getDetail();
        String address = provinceName+cityName+detail;
        System.out.println(address);
        Object distance = baiduLocationUtil.Distance(address);
        if ((int)distance>5000){
            throw new BusinessException(MessageConstant.DELIVERY_OUT_OF_RANGE);
        }

        //1.封装数据插入 orders表返回id
        orders.setNumber(String.valueOf(System.nanoTime()));
        orders.setStatus(1);
        orders.setUserId(BaseContext.getCurrentId());
        orders.setPayMethod(1);
        orders.setOrderTime(LocalDateTime.now());
        orders.setPhone(addressBook.getPhone());
        orders.setAddress(addressBook.getDetail());
        orders.setConsignee(addressBook.getConsignee());
        orders.setPayStatus(0);
        odersMapper.insert(orders);

        //2.根据购物车id查询菜品和套餐存入detail表
        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectById(shoppingCart);
        if (shoppingCartList != null){
            shoppingCartList.forEach( shoppingCart1 -> {
                OrderDetail orderDetail = BeanHelper.copyProperties(shoppingCart1, OrderDetail.class);
                orderDetail.setOrderId(orders.getId());
                odersDetailMapper.insert(orderDetail);
            });
        }

        //清空购物车
        shoppingCartMapper.delCartList(shoppingCart);

        //根据查询orders表返回id组装数据

        OrderSubmitVO submitVO = OrderSubmitVO.builder().id(orders.getId()).orderAmount(orders.getAmount()).orderNumber(orders.getNumber())
                .orderTime(orders.getOrderTime()).build();

        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("type",1);
        paramMap.put("orderId",orders.getId());
        paramMap.put("content","订单号"+orders.getNumber());
        webScoketServer.SendMessage(JSONObject.toJSONString(paramMap));
        return submitVO;
    }

    @Override
    public PageResult findHistoryOrders(OrdersPageQueryDTO ordersPageQueryDTO) {
        PageHelper.startPage(ordersPageQueryDTO.getPage(),ordersPageQueryDTO.getPageSize());

        //查询订单列表
        Orders orders = BeanHelper.copyProperties(ordersPageQueryDTO, Orders.class);
        orders.setUserId(BaseContext.getCurrentId());
        List<OrderVO> ordersList = odersMapper.findOrdersList(orders);
        StringBuilder orderDishes = new StringBuilder();
        for (OrderVO orderVO : ordersList) {
            List<OrderDetail> orderDetailList = orderVO.getOrderDetailList();

            orderDetailList.forEach(orderDetail -> {
                String number = orderDetail.getNumber().toString();
                String name = orderDetail.getName();

                orderDishes.append(name+"*"+number+",");

            });
            orderVO.setOrderDishes(orderDishes.toString());
        }

        Page<OrderVO> page = (Page<OrderVO>) ordersList;
        return new PageResult(page.getTotal(),page.getResult());

    }

    @Override
    public OrderVO QueryOrder(Long id) {
        Orders orders = Orders.builder().id(id).userId(BaseContext.getCurrentId()).build();
        List<OrderVO> ordersList = odersMapper.findOrdersList(orders);
        if (ordersList != null){
            OrderVO order1 = ordersList.get(0);
            return order1;
        }
        return null;
    }

    @Override
    public void addOrder(Long id) {

        List<OrderDetail> orderDetailList  = odersDetailMapper.findById(id);

            orderDetailList.forEach(orderDetail -> {
                ShoppingCart shoppingCart = BeanHelper.copyProperties(orderDetail, ShoppingCart.class);
                shoppingCart.setUserId(BaseContext.getCurrentId());
               shoppingCart.setCreateTime(LocalDateTime.now());
                shoppingCartMapper.insert(shoppingCart);
            });


    }

    @Override
    public void celOrders(Long id) {
        //校验订单是否存在
        Orders orders = Orders.builder().id(id).userId(BaseContext.getCurrentId()).build();
        List<OrderVO> ordersList = odersMapper.findOrdersList(orders);
        if (CollectionUtils.isEmpty(ordersList)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        //如果存在判断仅 待付款 和 待接单 状态的订单， 可以直接取消，其他不能取消
        OrderVO orderVO = ordersList.get(0);
        Integer status = orderVO.getStatus();
        Integer payStatus = orderVO.getPayStatus();
        if (status<=2 && payStatus==0 ){
            orderVO.setStatus(6);
            odersMapper.update(orderVO);
        }
        //如果订单状态为 待接单 且 支付状态为 已支付 的情况下, 还需要进行退款操作
        else if (status==2 && payStatus==1){

            //退款

            orderVO.setStatus(7);
            odersMapper.update(orderVO);
        }else {
            throw new BusinessException(MessageConstant.ORDER_HAS_PAID);
        }

        //最后取消订单，其实就是更新订单状态为 已取消
    }

    @Override
    public OrderStatisticsVO findOrderStatus() {
        OrderStatisticsVO  orderStatisticsVO = odersMapper.findOrderStatus();

          return orderStatisticsVO;
    }

    @Override
    public OrderVO findOrderDetail(Integer id) {

        Orders orders = Orders.builder().id(Long.valueOf(id)).build();
        List<OrderVO> orderVOList = odersMapper.findOrdersList(orders);
        if (CollectionUtils.isEmpty(orderVOList)){
            return null;
        }
        OrderVO orderVO = orderVOList.get(0);
        return orderVO;
    }

    @Override
    public void ConfirmOrders(Orders orders) {
        OrderVO orderVO = BeanHelper.copyProperties(orders, OrderVO.class);
        orderVO.setStatus(3);
        //默认此时付款
        orderVO.setPayStatus(1);
        orderVO.setCancelTime(LocalDateTime.now());
        odersMapper.update(orderVO);

    }

    @Override
    public void RejectionOder(OrdersRejectionDTO ordersRejectionDTO) {
        Orders order = BeanHelper.copyProperties(ordersRejectionDTO, Orders.class);
        //查询订单存在，且状态为2

        List<OrderVO> ordersList = odersMapper.findOrdersList(order);
        if (!CollectionUtils.isEmpty(ordersList)){
            OrderVO orderVO1 = ordersList.get(0);
            Integer status = orderVO1.getStatus();
            if (status==2){
                //此时可以拒单
             OrderVO orderVO2 = new OrderVO();
             orderVO2.setId(order.getId());
             orderVO2.setStatus(6);
             orderVO2.setRejectionReason(order.getRejectionReason());
            odersMapper.update(orderVO2);
            }else {
                throw new BusinessException("当前订单已付款，不能取消，只能退款");
                //退款
            }

        }else {
            throw new BusinessException("当前订单不存在");
        }

    }

    @Override
    public void cancelOrders(OrdersCancelDTO ordersCancelDTO) {
        //查询订单是否存在
        Orders order = BeanHelper.copyProperties(ordersCancelDTO, Orders.class);
        List<OrderVO> ordersList = odersMapper.findOrdersList(order);
        if (CollectionUtils.isEmpty(ordersList)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        OrderVO orderVO = ordersList.get(0);
        Integer status = orderVO.getStatus();
        Integer payStatus = orderVO.getPayStatus();
        if (payStatus==1){
            throw new BusinessException("顾客已支付，若取消订单请联系顾客");

        }
        if (status>=3){
            throw new BusinessException("顾客已支付，若取消订单请联系顾客");
        }
        OrderVO orderVO1 = new OrderVO();
        orderVO1.setId(order.getId());
        orderVO1.setCancelReason(order.getCancelReason());
        orderVO1.setStatus(6);
        orderVO1.setCancelTime(LocalDateTime.now());
        odersMapper.update(orderVO1);
    }

    @Override
    public void Delivery(Long id) {
        Orders order = Orders.builder().id(id).build();
        List<OrderVO> ordersList = odersMapper.findOrdersList(order);
        if (CollectionUtils.isEmpty(ordersList)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        OrderVO orderVO = ordersList.get(0);
        Integer Status = orderVO.getStatus();
        if (Status==3){
            OrderVO orderVO1 = new OrderVO();
            orderVO1.setId(id);
            orderVO1.setStatus(4);
            odersMapper.update(orderVO1);
        }
    }

    @Override
    public void CompleteOrders(Long id) {
        //查找订单是否存在
        Orders order = Orders.builder().id(id).build();
        List<OrderVO> ordersList = odersMapper.findOrdersList(order);
        if (CollectionUtils.isEmpty(ordersList)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        //判断订单状态
        OrderVO orderVO = ordersList.get(0);
        Integer Status = orderVO.getStatus();
        if (Status==4){
            OrderVO orderVO1 = new OrderVO();
            orderVO1.setId(id);
            orderVO1.setDeliveryTime(LocalDateTime.now());
            orderVO1.setStatus(5);
            odersMapper.update(orderVO1);
        }

    }

}
