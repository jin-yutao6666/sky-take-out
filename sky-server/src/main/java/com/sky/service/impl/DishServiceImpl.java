package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.*;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class DishServiceImpl implements DishService {

    @Autowired
    DishMapper dishMapper;
    @Autowired
    DishFlavorsMapper dishFlavorsMapper;
    @Autowired
    SetmealDishMapper setmealDishMapper;
    @Autowired
    SetmealMapper setmealMapper;
    @Autowired
    RedisTemplate<Object,Object> redisTemplate;


    @Transactional  //操作多张表
    @Override
    public void save(DishDTO dishDTO) {
        //1.插入菜品表
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        dish.setStatus(StatusConstant.DISABLE);
        dishMapper.save(dish);

        //2.插入口味表
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if (!CollectionUtils.isEmpty(flavors)) {
            flavors.forEach(flavor -> {
                flavor.setDishId(dish.getId());
            });
            dishFlavorsMapper.save(flavors);
        }
        //清理缓存
        Set<Object> keys = redisTemplate.keys("dish:*");
        redisTemplate.delete(keys);
    }

   // @Cacheable(cacheNames = "cache:dish",key = "#a0.name and #a0.categoryId and #a0.status")
    @Override
    public PageResult findDish(DishPageQueryDTO dishPageQueryDTO) {
        int page = dishPageQueryDTO.getPage();
        int pageSize = dishPageQueryDTO.getPageSize();
        PageHelper.startPage(page,pageSize);

        List<DishVO> dishVOList = dishMapper.findDish(dishPageQueryDTO);

        Page<DishVO> page1 = (Page<DishVO>) dishVOList;
        return new PageResult(page1.getTotal(),page1.getResult());
    }


    @Override
    public void delDish(List<Long> ids) {

        //1、菜品启售状态下不可售卖
        Long Count = dishMapper.findDishStastus(ids);

        if (Count > 0){
            throw new BusinessException(MessageConstant.DISH_ON_SALE);
        }

        //被套餐关联菜品不可售卖
           List<Long> dishId = setmealDishMapper.findList(ids);
            if (!dishId.isEmpty()){
                throw new BusinessException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
            }


        //3.删除菜品同时要删除菜品对应口味
        dishMapper.delDish(ids);
        dishFlavorsMapper.delDishFlavor(ids);
        //清理缓存
        Set<Object> keys = redisTemplate.keys("dish:*");
        redisTemplate.delete(keys);

    }

    @Override
    public DishVO findDishAndFlavors(Integer id) {
        //1.查询dish表返回 菜品基本数据
        DishVO dishVO=dishMapper.findDishById(id);

        //2.查询dishFlavors 表返回口味数据
        List<DishFlavor>  dishFlavorList =dishFlavorsMapper.findDishFlavors(id);

        //3.拼装数据
        dishVO.setFlavors(dishFlavorList);
        return dishVO;
    }



    @Transactional
    @Override
    public void updateDish(DishDTO dishDTO) {
        //更新dish表字段
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        dishMapper.updateDish(dish);

        //dishFlavor表先删除 后添加
        Long id = dishDTO.getId();
        List<Long> ids = new ArrayList<>();
        ids.add(id);
        dishFlavorsMapper.delDishFlavor(ids);
        List<DishFlavor> flavors = dishDTO.getFlavors();
        flavors.forEach(flavor->{
            flavor.setDishId(id);
        });
        dishFlavorsMapper.save(flavors);

        //清理缓存
        Set<Object> keys = redisTemplate.keys("dish:"+id);
        redisTemplate.delete(keys);
    }

    @Override
    public void dishStatus(Integer status, Long id) {
        //修改菜品状态
        Dish dish = new Dish();
        dish.setStatus(status);
        dish.setId(id);
        dishMapper.updateDish(dish);

        //判断菜品状态
        if (status==0){
            //此时 菜品是停售状态 就把套餐也停售

            //1.查询此菜品关联套餐id
         Long SetmealId =  setmealDishMapper.findSetmealId(id);
            //2.将改套餐设为停售
            setmealMapper.updatestatusBySetmealId(SetmealId,status);

        }

        //清理缓存
        Set<Object> keys = redisTemplate.keys("dish:*");
        redisTemplate.delete(keys);


    }

    @Override
    public List<Dish> findList(DishPageQueryDTO dishPageQueryDTO) {
        List<Dish> dishList = dishMapper.findList(dishPageQueryDTO);
        return dishList;
    }
}
