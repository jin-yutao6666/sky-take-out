package com.sky.service.impl;


import com.sky.service.ShopStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class ShopStatusServiceImpl implements ShopStatusService {

    @Autowired
    RedisTemplate redisTemplate;

    //设置店铺
    @Override
    public void setStatus(Integer status) {
        redisTemplate.opsForValue().set("shop",1);
        redisTemplate.opsForValue().set("shop",status);
    }

    @Override
    public Integer find() {
        Integer status =(Integer) redisTemplate.opsForValue().get("shop");


        return status;
    }
}
