package com.sky.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.BusinessException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;
    @Autowired
    WeChatProperties weChatProperties;

    @Override
    public User login(UserLoginDTO userLoginDTO) {

        //1.连接微信服务器获取返回OpenId
        Map<String,String> paramMap = new HashMap<>();
        paramMap.put("appid",weChatProperties.getAppid());
        paramMap.put("secret",weChatProperties.getSecret());
        paramMap.put("js_code",userLoginDTO.getCode());
        paramMap.put("grant_type","authorization_code");
        String get = HttpClientUtil.doGet("https://api.weixin.qq.com/sns/jscode2session", paramMap);
        JSONObject jsonObject = JSON.parseObject(get);
        String openid = jsonObject.getString("openid");


        //判断openid是否为空，如果为空表示登录失败，抛出业务业务
        if(StringUtils.isEmpty(openid)){
            throw new BusinessException(MessageConstant.LOGIN_FAILED);
        }

        //2.去数据库查找 是否存在openId （不存在自动注册）
        User user = userMapper.findOpenId(openid);
        if (user == null ){
            user = User.builder().openid(openid).createTime(LocalDateTime.now()).build();
            userMapper.insert(user);
        }


        //3.封装数据返回

        return user;
    }
}
