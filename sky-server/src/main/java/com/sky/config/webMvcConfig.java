package com.sky.config;


import com.sky.interceptor.AdminTokenIntercetor;
import com.sky.interceptor.UserTokenIntercetor;
import com.sky.json.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.cbor.MappingJackson2CborHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
@Slf4j
@Configuration
public class webMvcConfig implements WebMvcConfigurer {

    @Autowired
    AdminTokenIntercetor adminTokenIntercetor;

    @Autowired
    UserTokenIntercetor userTokenIntercetor;

    @Override//添加拦截器
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(adminTokenIntercetor).addPathPatterns("/admin/**").excludePathPatterns("/admin/employee/login");
        registry.addInterceptor(userTokenIntercetor).addPathPatterns("/user/**").excludePathPatterns("/user/user/login","/user/shop/status");
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
            log.info("扩展spring框架的消息转换器，在转换josn格式的数据时，使用自定义转换器");

        MappingJackson2HttpMessageConverter  messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setObjectMapper(new JacksonObjectMapper());

        converters.add(0,messageConverter);
      }
}
