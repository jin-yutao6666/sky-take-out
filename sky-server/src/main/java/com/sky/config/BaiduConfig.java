package com.sky.config;

import com.sky.properties.BaiduProperties;
import com.sky.utils.BaiduLocationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaiduConfig {

    @Bean
    public BaiduLocationUtil baiduLocationUtil(BaiduProperties baiduProperties){
        return new BaiduLocationUtil(baiduProperties);
    }
}
