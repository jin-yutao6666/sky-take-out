package com.sky.aspect;


import com.alibaba.druid.util.StringUtils;
import com.sky.autofill.AutoFill;
import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Aspect
@Slf4j
@Component
public class AutoFillAspect {

    @Before("execution(* com.sky.mapper.*.*(..)) && @annotation(com.sky.autofill.AutoFill)")
    public void autoFill(JoinPoint joinPoint) throws Exception {
        //1.获得原始方法传入参数
        Object[] args = joinPoint.getArgs();
        if (ObjectUtils.isEmpty(args)) {
            log.info("{} 内容为空", args);
            return;
        }

        log.info("开始前...{}", args);
        //2、通过反射获取到对象对应方法 【4个】公共字段
        Object o = args[0];
        Method createTime = o.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class);
        Method updateTime = o.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
        Method createUser = o.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class);
        Method updateUser = o.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);

        //3.获取注解对应的value属性
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        OperationType operationType = methodSignature.getMethod().getAnnotation(AutoFill.class).value();

        //4、判断 如果是insert 为4个属性
        if (operationType.equals(OperationType.INSERT)) {
            createTime.invoke(o, LocalDateTime.now());
            createUser.invoke(o, BaseContext.getCurrentId());
        }

        updateTime.invoke(o, LocalDateTime.now());
        updateUser.invoke(o, BaseContext.getCurrentId());
        log.info("结束进行公共属性的自动填充...{}", args);

    }
}
