package com.sky.interceptor;

import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.ibatis.plugin.Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@Slf4j
@Component
public class AdminTokenIntercetor<postHandle> implements HandlerInterceptor {

    @Autowired
    JwtProperties jwtProperties;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.info("拦截请求{}",request.getRequestURL().toString());
       //1.获得token
        String  jwt = request.getHeader(jwtProperties.getAdminTokenName());

        //2. 判断令牌是否存在, 如果不存在, 响应401状态码
        if (!StringUtils.hasLength(jwt)){
            log.info("令牌不存在");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }

        //3. 如果令牌存在, 校验令牌是否合法, 不合法, 响应401状态码
        try {
            //解析token获得map结果
            Claims claims = JwtUtil.parseJWT(jwtProperties.getAdminSecretKey(), jwt);
            Long empId = Long.valueOf(claims.get("id").toString());
            //放入ThreadLocal 线程隔离
            BaseContext.setCurrentId(empId);
            //放行
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("token解析非法");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }


    }

    //释放线程资源


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        BaseContext.removeCurrentId();
    }
}
