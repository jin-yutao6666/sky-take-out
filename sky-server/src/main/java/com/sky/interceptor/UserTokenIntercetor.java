package com.sky.interceptor;

import com.alibaba.druid.util.StringUtils;
import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class UserTokenIntercetor implements HandlerInterceptor {

    @Autowired
    JwtProperties jwtProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String jwt = request.getHeader(jwtProperties.getUserTokenName());

        //2.如果令牌不存在 响应401
        if (StringUtils.isEmpty(jwt)){
            log.info("jwt为空");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }

        //3.如果令牌解析错误 响应401
        try {
            Claims claims = JwtUtil.parseJWT(jwtProperties.getUserSecretKey(), jwt);
            Long id = Long.valueOf(claims.get("id").toString());
            BaseContext.setCurrentId(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("令牌解析错误");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;

        }


    }

    //释放资源

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        BaseContext.removeCurrentId();
    }
}
