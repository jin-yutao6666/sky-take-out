package com.sky.controller.shop;

import com.sky.result.Result;
import com.sky.service.ShopStatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "店铺状态")
@RestController
@RequestMapping("/admin/shop")
public class ShopStatusController {

    @Autowired
    ShopStatusService shopStatusService;

    @ApiOperation(value = "设置店铺状态")
    @PutMapping("/{status}")
    public Result setStatus(@PathVariable Integer status){
        shopStatusService.setStatus(status);
        return Result.success();
    }

    @ApiOperation(value = "查询店铺状态")
    @GetMapping("/status")
    public Result findStatus(){
        Integer status =shopStatusService.find();
        return Result.success(status);
    }


}
