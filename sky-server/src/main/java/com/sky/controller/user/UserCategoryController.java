package com.sky.controller.user;


import com.sky.dto.CategoryDTO;
import com.sky.entity.Category;
import com.sky.result.Result;
import com.sky.service.UserCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "查询分类")
@RestController
@RequestMapping("/user/category")
public class UserCategoryController {

    @Autowired
    UserCategoryService userCategoryService;

    @ApiOperation(value = "条件查询分类")
    @GetMapping("/list")
    public Result categoryList(CategoryDTO categoryDTO){
        List<Category> categoryList  =userCategoryService.categoryList(categoryDTO);
        return Result.success(categoryList);
    }
}
