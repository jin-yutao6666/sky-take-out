package com.sky.controller.user;


import com.sky.dto.DishDTO;
import com.sky.entity.Dish;
import com.sky.result.Result;
import com.sky.service.UserDishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "小程序端查询菜品")
@RequestMapping("/user/dish")
@RestController
public class UserDishController {

    @Autowired
    UserDishService userDishService;

    @ApiOperation("菜品查询")
    @GetMapping("/list")
    public Result UserDishList(Long categoryId){
        List<DishVO> dishList =userDishService.UserDishList(categoryId);
        return Result.success(dishList);
    }
}
