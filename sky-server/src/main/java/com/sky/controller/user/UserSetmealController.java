package com.sky.controller.user;

import com.sky.dto.SetmealDTO;
import com.sky.entity.Setmeal;
import com.sky.result.Result;
import com.sky.service.UserSetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "小程序端套餐管理")
@RestController
@RequestMapping("/user/setmeal")
public class UserSetmealController {

    @Autowired
    UserSetmealService userSetmealService;

    @ApiOperation(value = "查询套餐")
    @GetMapping("/list")
    public Result setmealList(SetmealDTO setmealDTO){

        List<Setmeal> setmealList =userSetmealService.setmealList(setmealDTO);
            return Result.success(setmealList);
    }

    @ApiOperation(value = "根据套餐id查菜品")
    @GetMapping("/dish/{id}")
    public Result setmealDishList(@PathVariable Long id){
        List<DishItemVO> dishItemVOlist = userSetmealService.setmealDishList(id);
       return Result.success(dishItemVOlist);
    }
}
