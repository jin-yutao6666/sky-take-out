package com.sky.controller.user;

import com.sky.entity.AddressBook;
import com.sky.result.Result;
import com.sky.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/addressBook")
public class AddressBookController {
    @Autowired
    AddressBookService addressBookService;
    @PostMapping
    public Result save(@RequestBody AddressBook addressBook){
            addressBookService.save(addressBook);
            return Result.success();
    }


    @GetMapping("/list")
    public Result findList(){
        List<AddressBook> addressBookList = addressBookService.findList();
        return Result.success(addressBookList);
    }

    //根据id查询地址
    @GetMapping("/{id}")
    public Result<AddressBook> findById(@PathVariable Long id){
        AddressBook addressBook = addressBookService.findById(id);
        return Result.success(addressBook);
    }

    //根据id修改地址
    @PutMapping
    public Result updateAddressList(@RequestBody AddressBook addressBook){
        addressBookService.updateAddressList(addressBook);
        return Result.success();
    }

    //根据id删除地址
    @DeleteMapping
    public Result delAddressList(Long id){
        addressBookService.delAddressList(id);
        return Result.success();
    }

    //设置默认地址
    @PutMapping("/default")
    public Result setDefult(@RequestBody AddressBook addressBook){
        addressBookService.setDefult(addressBook);
        return Result.success();
    }

    //查询默认地址
    @GetMapping("/default")
    public Result<AddressBook> findDefult(){
        AddressBook addressBook=addressBookService.findDefult();
        return Result.success(addressBook);
    }
}
