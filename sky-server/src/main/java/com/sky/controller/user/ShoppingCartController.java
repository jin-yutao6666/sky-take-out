package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCartService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "小程序购物车")
@Slf4j
@RestController
@RequestMapping("/user/shoppingCart")
public class ShoppingCartController {

    @Autowired
    ShoppingCartService shoppingCartService;

    @PostMapping("/add")
    public Result save(@RequestBody ShoppingCartDTO shoppingCartDTO){

        shoppingCartService.save(shoppingCartDTO);
        return Result.success();

    }

    @GetMapping("/list")
    public Result cartList(){
        List<ShoppingCart> shoppingCartList = shoppingCartService.find();
        return Result.success(shoppingCartList);
    }

    //删除购物车一个对象

    @PostMapping("/sub")
    public Result updateCartList(@RequestBody ShoppingCart shoppingCart){
        shoppingCartService.updateCartList(shoppingCart);
        return Result.success();
    }


    //清空购物车
    @DeleteMapping("clean")
    public Result delCartList(){
        shoppingCartService.delCartList();
        return Result.success();
    }

}
