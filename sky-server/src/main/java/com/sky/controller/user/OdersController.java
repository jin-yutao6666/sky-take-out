package com.sky.controller.user;

import com.alibaba.fastjson.JSONObject;
import com.sky.controller.WebScoketServer;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OdersService;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Api(tags = "C端，订单管理")
@RestController
@RequestMapping("/user/order")
public class OdersController {
    @Autowired
    OdersService odersService;
    @Autowired
    WebScoketServer webScoketServer;

    @ApiOperation(value = "提交订单")
    @PostMapping("/submit")
    public Result<OrderSubmitVO> OderSubmit(@RequestBody Orders  orders){
         OrderSubmitVO orderSubmitVO =  odersService.OderSubmit(orders);
         return Result.success(orderSubmitVO);
    }

    @GetMapping("/historyOrders")
    public Result<PageResult> findHistoryOrders(OrdersPageQueryDTO ordersPageQueryDTO){
        PageResult  pageResult  = odersService.findHistoryOrders(ordersPageQueryDTO);
        return Result.success(pageResult);
    }

    @GetMapping("/orderDetail/{id}")
    public Result<OrderVO> QueryOrder(@PathVariable Long id){
       OrderVO orderVO = odersService.QueryOrder(id);
       return Result.success(orderVO);

    }

    //取消订单
   @PutMapping("/cancel/{id}")
   public Result celOrders(@PathVariable Long id){
       odersService.celOrders(id);
       return Result.success();
   }


    //再来一单
    @PostMapping("/repetition/{id}")
    public Result addOrder(@PathVariable Long id){
        odersService.addOrder(id);
        return Result.success();
    }

    //催单
    @GetMapping("/reminder/{id}")
    public Result reminder(@PathVariable Long id){
        OrderVO orderVO = odersService.QueryOrder(id);
        String number = orderVO.getNumber();
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("type",2);
        paramMap.put("orderId",orderVO.getId());
        paramMap.put("content","订单号"+number);
        webScoketServer.SendMessage(JSONObject.toJSONString(paramMap));
        return Result.success();
    }

}
