package com.sky.controller.setmeal;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "套餐管理")
@RequestMapping("/admin/setmeal")
@RestController
public class SetmealController {

    @Autowired
    SetmealService setmealService;

    //新增套餐
    @ApiOperation("新增套餐")
    @PostMapping
    public Result save(@RequestBody SetmealDTO setmealDTO){
        setmealService.save(setmealDTO);
        return Result.success();
    }


    //分页查询
    @ApiOperation("新增套餐")
    @GetMapping("/page")
    public Result<PageResult> SetmealList(SetmealPageQueryDTO setmealPageQueryDTO){
        PageResult pageResult = setmealService.SetmealList(setmealPageQueryDTO);
        return Result.success(pageResult);
    }

    //批量删除套餐
    @ApiOperation("批量删除套餐")
    @DeleteMapping
    public Result delSetmeal(@RequestParam List<Integer> ids){
        setmealService.delSetmeal(ids);
        return Result.success();
    }

    //根据id查询套餐
    @ApiOperation(value = "根据id查询套餐")
    @GetMapping("/{id}")
    public Result findSetmeal(@PathVariable Integer id){
        SetmealVO setmealVO = setmealService.findSetmeal(id);
        return Result.success(setmealVO);

    }

    //.修改套餐
    @ApiOperation(value = "修改套餐")
    @PutMapping
    public Result updateSetmeal(@RequestBody SetmealDTO setmealDTO){
            setmealService.updateSetmeal(setmealDTO);
            return Result.success();

    }


    //套餐起售、停售
    @ApiOperation(value = "套餐起售、停售")
    @PutMapping("/status/{status}/{id}")
    public Result ableAndDisable(@PathVariable Integer status,@PathVariable Long id){
        setmealService.ableAndDisable(status,id);
        return Result.success();
    }


}
