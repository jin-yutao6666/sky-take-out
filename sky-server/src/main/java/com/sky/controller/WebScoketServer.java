package com.sky.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@ServerEndpoint("/ws/{sid}")//标识当前类是处理webSocket请求
public class WebScoketServer {

        private static Map<String,Session> sessionMap = new HashMap<>();
    @OnOpen
    public  void open(Session session, @PathParam("sid") String sid){
        log.info("建立连接，{}",sid);
        sessionMap.put(sid,session);
    }
    @OnMessage
    public  void Message(Session session, String message, @PathParam("sid") String sid){
            log.info("接收到客户端消息{}",message);

    }
    @OnClose
    public  void close(Session session, @PathParam("sid") String sid){

        log.info("关闭连接。。。");
        sessionMap.remove(sid);
    }
    @OnError
    public  void Error(Session session, @PathParam("sid") String sid,Throwable throwable){

    }


    public void SendMessage(String message){
        Collection<Session> values = sessionMap.values();
        values.forEach(value->{
            try {
                value.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
