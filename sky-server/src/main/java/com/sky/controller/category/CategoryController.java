package com.sky.controller.category;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "菜品管理")
@RestController
@RequestMapping("/admin/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @ApiOperation(value = "新增分类")
    @PostMapping
    public Result save(@RequestBody CategoryDTO categoryDTO){
        categoryService.save(categoryDTO);
        return Result.success();
    }

    //分类分页查询
    @ApiOperation(value = "分类分页查询")
    @GetMapping("/page")
    public Result<PageResult> findCategoryList(CategoryPageQueryDTO categoryPageQueryDTO) {
      log.info("查询分类分页 {}",categoryPageQueryDTO);
        PageResult pageResult =  categoryService.findCategoryList(categoryPageQueryDTO);
            return Result.success(pageResult);
    }


    //根据ID查询分类信息

    @ApiOperation(value ="根据ID查询分类信息")
    @GetMapping("/{id}")
    public Result findCategoryId(@PathVariable Long id){
        log.info("根据ID查询分类信息 {}",id);

       Category category = categoryService.findCategoryId(id);
       return Result.success(category);
    }

    //修改分类
    @ApiOperation(value = "修改分类")
    @PutMapping
    public Result updateCategroy(@RequestBody CategoryDTO categoryDTO){
        categoryService.updateCategroy(categoryDTO);
        return Result.success();
    }

    //启用、禁用分类
    @ApiOperation(value = "启用、禁用分类")
    @PutMapping("/status/{status}/{id}")
    public Result ableAndDisable(@PathVariable Integer status,@PathVariable Long id){
        log.info("启用、禁用分类 status {},id {}",status,id);
            categoryService.ableAndDisable(status,id);
            return Result.success();
    }

    //根据id删除分类
    @ApiOperation(value = "根据id删除分类")
    @DeleteMapping("/{id}")
    public Result delCategroy(@PathVariable Long id){
        categoryService.delCategroy(id);
        return Result.success();
    }

    //根据类型查询分类
    @ApiOperation(value = "根据类型查询分类")
    @GetMapping("/list")
    public  Result findCategoryByType(Integer type){
        List<Category> categoryList  = categoryService.findCategoryByType(type);
        return Result.success(categoryList) ;
    }

}
