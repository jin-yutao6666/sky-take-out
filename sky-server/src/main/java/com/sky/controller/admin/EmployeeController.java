package com.sky.controller.admin;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 员工管理Controller
 */
@Slf4j
@Api(tags = "员工接口")
@RestController
@RequestMapping("/admin/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    JwtProperties jwtProperties;
    @ApiOperation(value = "员工登录接口")
    @PostMapping("/login")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO){

       Employee employee = employeeService.login(employeeLoginDTO);

       Map<String,Object> claims = new HashMap<>();
       //id 用户唯一标识
       claims.put("id",employee.getId());

       //生成jwt
        String jwt = JwtUtil.createJWT(jwtProperties.getAdminSecretKey(), jwtProperties.getAdminTtl(), claims);
        //封装数据
        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .name(employee.getName())
                .userName(employee.getUsername())
                .token(jwt).build();

        return Result.success(employeeLoginVO);
    }


    //新增员工
    @ApiOperation(value = "新增员工接口")
    @PostMapping
    public Result save(@RequestBody EmployeeDTO employeeDTO){
        log.info("新增员工信息，{}",employeeDTO);
        employeeService.save(employeeDTO);
        return Result.success();
    }

    //查询员工
    @ApiOperation(value = "查询员工")
    @GetMapping("/page")
    public Result<PageResult> findEmpList(EmployeePageQueryDTO pageQueryDTO ){
        log.info("查询员工 {}" ,pageQueryDTO);
        PageResult pageResult = employeeService.findEmpList(pageQueryDTO);
        return Result.success(pageResult);
    }

    //禁用或启用
    @ApiOperation(value = "禁用或启用")
    @PutMapping("/status/{status}/{id}")
    public Result ableAndDisable(@PathVariable Integer status,@PathVariable Long id){
       log.info("启动或禁用员工账户 status:{} id:{} ",status,id);
        employeeService.changeStatus(status,id);
        return Result.success();

    }


    //查询回显
    @ApiOperation("查询回显id")
    @GetMapping("/{id}")
    public Result findEmpId(@PathVariable Long id){
        log.info("查询回显 id {}",id);
        Employee employee = employeeService.findById(id);
        return Result.success(employee);
    }



    //修改员工
    @ApiOperation("修改员工")
    @PutMapping
    public Result updateEmp(@RequestBody Employee employee){
        employeeService.updateEmp(employee);
        return Result.success();
    }

    //修改员工密码
    @ApiOperation("修改员工密码")
    @PutMapping("/editPassword")
    public Result updatePassword(@RequestBody PasswordEditDTO passwordEditDTO){
        log.info("传入修改密码参数 {}",passwordEditDTO);
        employeeService.updatePassword(passwordEditDTO);
        return Result.success();
    }
}
