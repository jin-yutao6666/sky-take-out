package com.sky.controller.admin;

import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.entity.Orders;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OdersService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/order")
public class AdminOrderController {

    @Autowired
    OdersService odersService;

    @GetMapping("/conditionSearch")
    public Result<PageResult> orderList(OrdersPageQueryDTO ordersPageQueryDTO){
        PageResult historyOrders = odersService.findHistoryOrders(ordersPageQueryDTO);
        return Result.success(historyOrders);
    }

    @GetMapping("/statistics")
    public Result<OrderStatisticsVO> OrderStutas(){
        OrderStatisticsVO orderStatisticsVO =odersService.findOrderStatus();
        return Result.success(orderStatisticsVO);
    }

    @GetMapping("/details/{id}")
    public Result<OrderVO> findOrderDetail(@PathVariable Integer id){
        OrderVO orderVO = odersService.findOrderDetail(id);
        return Result.success(orderVO);
    }

    @PutMapping("/confirm")
    public Result ConfirmOrders(@RequestBody Orders orders){
        odersService.ConfirmOrders(orders);
        return Result.success();
    }

    @PutMapping("/rejection")
    public Result RejectionOder(@RequestBody OrdersRejectionDTO ordersRejectionDTO){
        odersService.RejectionOder(ordersRejectionDTO);
        return Result.success();
    }

    @PutMapping("/cancel")
    public Result celOrders(@RequestBody OrdersCancelDTO ordersCancelDTO){
        odersService.cancelOrders(ordersCancelDTO);
        return Result.success();
    }

    //派送订单
    @PutMapping("/delivery/{id}")
    public Result Delivery(@PathVariable Long id){
        odersService.Delivery(id);
        return Result.success();
    }

    //完成订单
    @PutMapping("/complete/{id}")
    public Result CompleteOrders(@PathVariable Long id){
        odersService.CompleteOrders(id);
        return Result.success();
    }
}
