package com.sky.controller.task;

import com.sky.entity.Orders;
import com.sky.mapper.OdersMapper;
import com.sky.utils.BeanHelper;
import com.sky.vo.OrderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
public class OrderTask {

    @Autowired
    OdersMapper odersMapper;

    @Scheduled(cron = "0/30 * * * * ?")
    public void OrderOvertimeTask(){
        //1.查看订单列表是否存在15分钟之前的订单
        LocalDateTime OrderTimeBefore = LocalDateTime.now().minusMinutes(15);
           List<Orders> ordersList =odersMapper.findOrderTimeBefore(1,OrderTimeBefore);
        //2.取消定单

        if (!CollectionUtils.isEmpty(ordersList)){
            ordersList.forEach(orders -> {
                log.info("订单{}超时,系统自动取消",orders);
                orders.setStatus(Orders.ORDER_STAUTS_CANCELLED);
                orders.setCancelTime(LocalDateTime.now());
                orders.setCancelReason("订单超时，自动取消");
                OrderVO orderVO = BeanHelper.copyProperties(orders, OrderVO.class);
                odersMapper.update(orderVO);
            });
        }
    }



    //定时完成订单
    @Scheduled(cron = "0 0 1 * * ?")
    public void FineshOrder(){
        //1.查看订单列表是否存在15分钟之前的订单
        LocalDateTime OrderTimeBefore = LocalDateTime.now().minusHours(2);
        List<Orders> ordersList =odersMapper.findOrderTimeBefore(4,OrderTimeBefore);
        //2.取消定单

        if (!CollectionUtils.isEmpty(ordersList)){
            ordersList.forEach(orders -> {
                log.info("订单{}，已接单超过2小时，自动更新已完成",orders);
                orders.setStatus(5);
                orders.setDeliveryTime(LocalDateTime.now());
                OrderVO orderVO = BeanHelper.copyProperties(orders, OrderVO.class);
                odersMapper.update(orderVO);
            });
        }
    }
}
