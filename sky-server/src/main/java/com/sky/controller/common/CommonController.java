package com.sky.controller.common;



import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/admin/common")
public class CommonController {

    @Autowired
    AliOssUtil aliOssUtil;

    @PostMapping("/upload")
    public Result uploadFile(MultipartFile file) throws Exception {

            log.info("上传文件名为{}" ,file.getOriginalFilename());
        String originalFilename = file.getOriginalFilename();
        String extName = file.getOriginalFilename().substring(originalFilename.lastIndexOf("."));
        String objName = UUID.randomUUID().toString()+extName;
        String url = aliOssUtil.upload(file.getBytes(), objName);

        log.info("上传文件名为{}" ,url);
        return Result.success(url);
    }


}
