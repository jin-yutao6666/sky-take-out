package com.sky.controller.workspace;

import com.sky.result.Result;
import com.sky.service.AdminWorkSpaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "工作台")
@Slf4j
@RestController
@RequestMapping("/admin/workspace")
public class AdminWorkSpace {


    @Autowired
    AdminWorkSpaceService adminWorkSpaceService;

    @ApiOperation( value = "今日运营数据")
    @GetMapping("/businessData")
    public Result<BusinessDataVO> businessData(){
        BusinessDataVO businessData = adminWorkSpaceService.businessData();
        return Result.success(businessData);
    }


    @ApiOperation( value = "查询今日订单数据")
    @GetMapping("/overviewOrders")
    public Result<OrderOverViewVO> overviewOrders(){
        OrderOverViewVO orderOverViewVO = adminWorkSpaceService.overviewOrders();
        return Result.success(orderOverViewVO);
    }


    @ApiOperation( value = "查询菜品总览")
    @GetMapping("/overviewDishes")
    public Result<DishOverViewVO> overviewDishes(){
        DishOverViewVO dishOverViewVO = adminWorkSpaceService.overviewDishes();
        return Result.success(dishOverViewVO);
    }

    @ApiOperation( value = "查询套餐总览")
    @GetMapping("/overviewSetmeals")
    public Result<SetmealOverViewVO> overviewSetmeals(){
        SetmealOverViewVO setmealOverViewVO = adminWorkSpaceService.overviewSetmeals();
        return Result.success(setmealOverViewVO);
    }
}
