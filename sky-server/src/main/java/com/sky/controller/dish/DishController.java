package com.sky.controller.dish;


import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "菜品管理")
@RestController
@RequestMapping("/admin/dish")
public class DishController {
    @Autowired
    private DishService dishService;

    //新增菜品
    @ApiOperation(value = "新增菜品")
    @PostMapping
    public Result save(@RequestBody DishDTO dishDTO){
        dishService.save(dishDTO);
        return Result.success();
    }

    //菜品分页查询
    @ApiOperation(value = "菜品分页查询")
    @GetMapping("/page")
    public  Result<PageResult> findDish(DishPageQueryDTO dishPageQueryDTO){
       PageResult pageResult = dishService.findDish(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    //批量删除菜品
    @ApiOperation(value = "批量删除菜品")
    @DeleteMapping
    public Result delDish(@RequestParam List<Long> ids){
        dishService.delDish(ids);
        return Result.success();
    }

    //根据id查询菜品
    @ApiOperation(value = "根据id查询菜品")
    @GetMapping("/{id}")
    public Result<DishVO> findDishAndFlavors(@PathVariable Integer id){
            DishVO dishVO = dishService.findDishAndFlavors(id);
            return Result.success(dishVO);
    }

    //修改菜品
    @ApiOperation(value = "修改菜品")
    @PutMapping
    public Result updateDish(@RequestBody DishDTO dishDTO){
            dishService.updateDish(dishDTO);
            return Result.success();
    }

    //菜品起售、停售
    @ApiOperation(value = "菜品起售、停售")
    @PutMapping("/status/{status}/{id}")
    public Result dishStatus(@PathVariable Integer status,@PathVariable Long id){
        dishService.dishStatus(status,id);
        return Result.success();

    }

    //条件查询菜品列表
    @ApiOperation(value = "条件查询菜品列表")
    @GetMapping("/list")
    public Result findList(DishPageQueryDTO dishPageQueryDTO){
           List<Dish> dishList = dishService.findList(dishPageQueryDTO);
            return Result.success(dishList);
    }



}
