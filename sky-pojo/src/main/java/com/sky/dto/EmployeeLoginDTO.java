package com.sky.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@ApiModel(value = "员工登录传递信息数据模型")
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeLoginDTO implements Serializable {

    @ApiModelProperty(value = "员工登录用户名")
    private String username;
    @ApiModelProperty(value = "员工登录密码")
    private String password;

}
