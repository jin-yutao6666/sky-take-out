package com.sky.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "sky.baidu")
public class BaiduProperties {

    private String ak;
    private String url;
    private String drivingurl;
    private String address;
}
