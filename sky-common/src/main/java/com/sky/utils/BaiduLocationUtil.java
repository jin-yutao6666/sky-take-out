package com.sky.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sky.entity.BaiduMap;
import com.sky.exception.BusinessException;
import com.sky.properties.BaiduProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.HashMap;

@Data
@AllArgsConstructor
@Slf4j
public class BaiduLocationUtil {

    BaiduProperties baiduProperties;


    public    Object  Distance( String Address2){

        BaiduMap baiduMap1 = SearchAk(baiduProperties.getAddress());
        String origin = baiduMap1.getLat()+","+baiduMap1.getLng();
        BaiduMap baiduMap2 = SearchAk(Address2);
        String destination = baiduMap2.getLat()+","+baiduMap2.getLng();


        HashMap claim = new HashMap();
        claim.put("origin",origin);
        claim.put("destination",destination);
        claim.put("ak",baiduProperties.getAk());
        String o =baiduProperties.getDrivingurl()+origin+destination+baiduProperties.getAk();
        System.out.println(o);
        String s = HttpClientUtil.doGet(baiduProperties.getDrivingurl(), claim);
        JSONObject jsonObject = JSON.parseObject(s);

        JSONArray routes = jsonObject.getJSONObject("result").getJSONArray("routes");
        JSONObject jsonObject1 = routes.getJSONObject(0);
        Object distance =  jsonObject1.get("distance");
       return distance;
    }

    public BaiduMap SearchAk(String Address){
        HashMap claim = new HashMap();
        claim.put("address",Address);
        claim.put("output", "json");
        claim.put("ak", baiduProperties.getAk());

        String Rs = HttpClientUtil.doGet(baiduProperties.getUrl(), claim);
        JSONObject location = JSON.parseObject(Rs).getJSONObject("result").getJSONObject("location");
        BigDecimal lng = (BigDecimal) location.get("lng");
        BigDecimal lat = (BigDecimal) location.get("lat");
        BaiduMap baiduMap = new BaiduMap();
        baiduMap.setLat(lat);
        baiduMap.setLng(lng);
        return baiduMap;
    }
}
